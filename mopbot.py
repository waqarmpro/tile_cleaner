"""
Simulation of a mop bot. Function mop uses helper functions to read information from a 
file that gives grid size, start position, move sequence, and walls. Tiles are in all 
positions except walls. Go through grid/room and mop tiles by following
the given move sequence. Return final position and number of tiles mopped at end of move sequence.
"""

__author__ = "Waqar Mahmood"

import os
import re

def mop(input_file):
    """ 
    Read information from input_file to create grid with start position and walls. Then move through
    grid per given move sequence. Return final position and number of tiles mopped.

    Input:
        1. String: input_file contains the name of a text file you need to read that is in the same directory
            This string contains the file name and extension: i.e. "input.txt"
    Outputs:
        1. Int: x coordinate of final position of mopbot
        2. Int: y coordinate of final position of mopbot
        3. Int: total of tiles cleaned by mopbot across all movements
    """

    #Read values provided by input file and store in corresponding variables:
    #The first 4 are values provided in input_file. The last (success) is a flag that indicates
    #whether values read from input_file are valid values.
    grid_dimensions, start_coordinates, move_sequence, walls, status, error_message = fetch_values_from_file(input_file)

    #confirm that returned flag says all values were valid, if not return error result
    if (not status == "OK"):
        return (-1, -1, 0, error_message)
    
    #get number of rows and columns to create grid.
    col_count = grid_dimensions[0]
    row_count = grid_dimensions[1]

    #create grid and initialize it. Returned grid has tiles and walls in all applicable spots    
    grid = create_grid(col_count, row_count, start_coordinates, walls)

    #get row and col number for starting position, initialize tile_count to 0
    final_pos_x = start_coordinates[0]
    final_pos_y = start_coordinates[1]
    tiles_cleaned = 0

    #Iterate through move sequence, update position and tiles cleaned with each iteration as needed
    for move in move_sequence:
        final_pos_x, final_pos_y, tiles_cleaned = process_move(grid, final_pos_x, final_pos_y, move, tiles_cleaned)

    return final_pos_x, final_pos_y, tiles_cleaned, error_message

def fetch_values_from_file(input_file_name):
    """
    Open file specified by argument, read information provided
    
    Input:
        1. String: name of file to be read. 
    Output:
        A tuple that has:
            1. Array: array of two ints that represents grid dimensions along x-axis and y-axis 
                (e.g. [5,4] for a 5 columns wide and 4 rows high grid).
            2. Array: array of two ints that represents start coordinates x and y.
            3. String: sequence of moves. One or more of any of N=north, E=east, W=west, S=south
            4. 2-D array: an array of wall coordinates themselves held in a two-element array of ints.
            5. String: status that has a value of either 'OK' or 'Error'.
            6. String: message about what gave the error.
    """

    #define error Tuple to return if invalid input detected
    #the error message is not currently used and is available for future use
    error_skeleton_tuple = ([],[],"",[])
    status = "OK"
    error_message = ""

    #read lines from file into an array

    lines, valid_IO = read_lines_from_file(input_file_name)

    if (not valid_IO):
        status = "Error"
        error_message = "File IO Error"
        return error_skeleton_tuple + (status, error_message)        

    #Input must have grid size, start position, and list of moves. So must have at least 3 lines. 
    if (len(lines) < 3):
        status = "Error"
        error_message = "File has Less than the minimum of three lines"
        return error_skeleton_tuple + (status, error_message)

    #Read first line, which is grid dimensions
    grid_dimensions = lines[0].strip().split(" ") #split the two values for coordinates into array

    #confirm they are valid coordinates. Assume neither grid dimension can be zero.
    if ( (not convert_to_pos_int(grid_dimensions)) or (grid_dimensions[0] == 0) or (grid_dimensions[1] == 0) ):
        status = "Error"
        error_message = "Invalid dimension"
        return error_skeleton_tuple + (status, error_message)
    
    #Assign max values possible for x and y to to named variables for readability
    max_x_value = grid_dimensions[0]-1
    max_y_value = grid_dimensions[1]-1

    #read second line, which is start coordinates
    start_coordinates = lines[1].strip().split(" ") #split the two values into array
    
    if (not convert_to_pos_int(start_coordinates) or 
        not coordinates_in_range(start_coordinates[0], start_coordinates[1], max_x_value, max_y_value)):
        status = "Error"
        error_message = "Invalid start position"
        return error_skeleton_tuple + (status, error_message)

    #read third line, which is move sequence
    #This code assumes that lowercase entries are permissible and accommodates regardless of case
    move_sequence = lines[2].strip().upper() #read line, strip end of line, convert to uppercase
    if (not check_valid_moves(move_sequence)): #check if all characters are valid moves
        status = "Error"
        error_message = "Invalid value for move sequence"
        return error_skeleton_tuple + (status, error_message)

    walls = [] #Variable walls holds an array of coordinates for the different walls

    #If there are more than three lines in file (i.e. wall info given)
    if (len(lines) > 3):
        #Parse walls. 
        wall_coordinates = [] #used to hold one set of coordinates while parsing through the list of them

        #start from line number three to end
        for i in range(3, len(lines)):
            wall_coordinates = lines[i].strip().split(" ") #get coordinates for current wall

            #check that they are valid coordinates and within bounds, and not atop start position
            if (not convert_to_pos_int(wall_coordinates) or 
                not coordinates_in_range(wall_coordinates[0], wall_coordinates[1], max_x_value, max_y_value) or
                wall_coordinates == start_coordinates):
                status = "Error"
                error_message = "Invalid wall coordinates"
                return error_skeleton_tuple + (status, error_message)
            else: #we have valid coordinates so append to walls
                walls.append(wall_coordinates)

    #at this point we have read all values from input_file and they are valid. Return them with a success status of OK.
    return grid_dimensions, start_coordinates, move_sequence, walls, "OK", ""


def read_lines_from_file(filename):
    """
    Opens, reads all lines from given file and returns the array of lines. Closes file.

    Input:
        1. String: name of file to be read.
    Output:
        1. Array of strings: an array that has all lines read from file.
        2. Boolean: a flag that reports whether file IO was successful.
    """

    #define path to file, construct filename with path
    path = os.getcwd()
    filename = path +r"/test_files/"+ filename

    #open file for read, read all lines in file into variable lines, close file. Return variable lines.
    try:
        file = open(filename, "r")
        lines = file.readlines()
        file.close()
        return lines, True
    except IOError:
        return [], False

def convert_to_pos_int(coordinates):
    """
    Convert coordinates that are read as string to positive integers: check that the
    characters are digits, only two coordinates provided, and that they are not negative.

    Input:
        1. Array: Coordinates that are passed as an array with two elements that are ints
    Output:
        2. Boolean: value that is true if valid coordinates
    """

    #check that exactly two values provided for coordinates, else return false
    if (not (len(coordinates) == 2)):
        return False

    #for each coordinate    
    for i, coordinate in enumerate(coordinates):
        #verify it is a positive digit. If so, overwrite with int value. 
        if (coordinate.isdigit()):
            coordinates[i] = int(coordinate)

            #If it is negative, return false
            if (coordinates[i] < 0):
                return False
        else: #return false if not a digit
            return False

    #if we have reached this point it is a valid coordinate. Return true.        
    return True

def coordinates_in_range(x_value, y_value, max_x_value, max_y_value):
    """
    Check that coordinates fall within grid dimensions.

    Input:
        1. Int: x value of coordinates.
        2. Int: y value of coordinates.
        3. Int: max possible x.
        4. Int: max possible y.
    Output:
        1. Boolean: true if coordinates are in range.
    """

    #confirm that coordinates are within max values
    if (x_value > max_x_value or y_value > max_y_value):
        return False
    else:
        return True

def check_valid_moves(move_seq):
    """
    Check that at least one move provided and characters in move sequence
    are valid characters. They must be N, E, W, or S.

    Input:
        1. String: has a sequence of move characters.
    Output:
        1. Boolean: reports true if valid move sequence.
    """

    #Must contain at least one move
    if (len(move_seq) == 0):
        return False

    #this regex call searches for a character other than NEWS. If no match, regex returns None
    if ( re.search('[^NEWS]', move_seq) == None ): #this means all characters are valid
        return True
    else:
        return False

def create_grid(col_count, row_count, start_pos, walls):
    """
    Create a 2D array defined with bounds defined by col_count, row_count.
    Set all values to 1 to indicate a tile present.
    Set start position to zero. Set all walls to -1
    
    Input:
        1. Int: column count
        2. Int: row count
        3. Array: array of two integers that are coordinates for start position
        4. Array: array of coordinates that are themselves an array of two integers
    Output:
        1. 2-D array: represents the grid initialized to its values.
    """

    #Create a 2-D array that initializes all values to 1
    grid = [[1 for i in range(col_count)] for j in range(row_count)]

    #set start position to 0 since base does not count toward tiles to be cleaned
    #The indexing  for 2-d array uses row first grid[row][col], Row corresponds to y-coordinate
    grid[start_pos[1]][start_pos[0]] = 0

    #for each wall, set that coordinate to -1 to indicate it's a wall. Nothing happens if walls empty.
    for wall in walls:
        grid[wall[1]][wall[0]] = -1 

    return grid

def process_move(grid, current_x, current_y, move, tile_count):
    """
        Process the current move. Update position and count tile as needed.
    
    Input:
        1. 2-D array: Grid.
        2. Int: current x coordinate.
        3. Int: current y coordinate.
        4. String: letter for the current move.
        5. Int: current tile count.
    Output:
        1. Int: post-move x coordinate (it will be unchanged if move attempted to move into a wall or border).
        2. Int: post-move y coordinate (same as above).
        3. Int: tile count.
    """

    #call get_post_move_coordinates to get new coordinates after processing move.
    #if move is not blocked by wall, we will be in a new spot
    new_x, new_y = get_post_move_coordinates_border(grid, current_x, current_y, move)

    #check if new spot has tile. If so, zero it out and increment tile count
    if (grid[new_y][new_x] == 1):
        grid[new_y][new_x] = 0
        tile_count += 1
    
    #return the values at end of this move    
    return new_x, new_y, tile_count

def get_post_move_coordinates_border(grid, current_x, current_y, move):
    """
    Get new coordinates based on attempted move. No change if move is blocked by wall or borders.
    This function assumes--as currently requried--that move into border is same as move into a wall.

    Input:
        1. 2-D array: grid.
        2. Int: current x coordinate
        3. Int: current y coordinate
        4. String: current letter for move
    Output:
        1. Int: new x-coordinate (maybe unchanged if move was into wall or border)
        2. Int: new y-coordinate
    """

    #calculate max values for x and y coordinates from grid dimensions
    max_x = len(grid[0])-1
    max_y = len(grid)-1

    #Check if new move is blocked by edge of grid or wall.
    if ( (move =="N") and (current_y < max_y) and (grid[current_y+1][current_x] != -1) ):
        current_y += 1 #if move North possible, increment y by 1

    elif ( (move =="E") and (current_x < max_x) and (grid[current_y][current_x+1] != -1) ):
        current_x += 1 #if move east possible, increment x by 1

    elif ( (move =="W") and (current_x > 0) and (grid[current_y][current_x-1] != -1) ):
        current_x -= 1 # if move west possible, decrement x by 1

    elif ( (move =="S") and (current_y > 0) and (grid[current_y-1][current_x] != -1) ):
        current_y -= 1 # if move south possible, decrement y by 1

    return current_x, current_y


def get_post_move_coordinates(grid, current_x, current_y, move):
    """
    Get new coordinates based on attempted move. No change if move is blocked by wall or borders.
    This function assumes that trying to go past border brings you back in through 
    opposite side, as happens in mopbot game - currently this function is not being used.
    Currently trying to go past border has same effect as trying to move into wall and so
    function get_post_move_coordinates_border is used instead of this one. This is for future use.

    Input:
        1. 2-D array: grid.
        2. Int: current x coordinate
        3. Int: current y coordinate
        4. String: current letter for move
    Output:
        1. Int: new x-coordinate (maybe unchanged if move was into wall or border)
        2. Int: new y-coordinate
    """

    #calculate max values for x and y coordinates from grid dimensions.
    max_x = len(grid[0])-1
    max_y = len(grid)-1

    #These variables will hold the spot that the move wants to take. The new spot
    #is then checked to see if it is a wall.
    target_x = -1
    target_y = -1

    #Based on direction specified by Move, as detailed by first case below (i.e. when move == "N").
    #Find target x or y this move wants to reach: 
    #If at border then we roll over, otherwise increment if moving N or E, decrement if moving W or S.
    #Update x (or y) to target x (or y) if that move is not blocked by a wall
    if (move == "N"): #attempt is to move up by one spot
        if (current_y < max_y):
            target_y = current_y+1 #if not at northern border then y increases by 1
        else:
            target_y = 0 #else if at northern edge then y rolls over

        #If the target y is not blocked by a wall, update current y to the target y    
        if (grid[target_y][current_x] != -1):
            current_y = target_y

    elif (move == "E"): #attempt is to move right by 1 spot
        if (current_x < max_x):
            target_x = current_x+1
        else:
            target_x = 0
        if (grid[current_y][target_x] != -1):
            current_x = target_x

    elif (move == "W"): #attempt is to move left by 1 spot
        if (current_x > 0):
            target_x = current_x-1
        else:
            target_x = max_x 
        if (grid[current_y][target_x] != -1):
            current_x = target_x

    elif (move == "S"): #attempt is to move down by one spot
        if (current_y > 0):
            target_y = current_y-1
        else:
            target_y = max_y
        if (grid[target_y][current_x] != -1):
            current_y = target_y

    return current_x, current_y



if __name__ == '__main__':

    """
    This code is executed only when this script is run directly. It is not executed when it
    is run as an import for unit tests.
    """

    #These calls were used to get the value for [submission.py] per instructions given to run
    #the script for unit tests: python mopbot.ut.py [submission.py]
    #print(sys.path)
    #print(os.getcwd())

    #Specify input file and print result
    input_file = "test_mopbot.txt"
    print(mop(input_file))