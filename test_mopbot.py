import unittest
import mopbot

class TestMopbot(unittest.TestCase):

    def test_fetch_values_from_file(self):

        #Test valid file with walls
        filename = "test_mopbot.txt"
        grid_dim, start_point, move_sequence, walls, status, error_msg = mopbot.fetch_values_from_file(filename)
        self.assertEqual(grid_dim, [5,5])
        self.assertEqual(start_point, [1,2])
        self.assertEqual(move_sequence, 'ENNNEWWNNSE')
        self.assertEqual(walls, [[1,0],[2,2]])
        self.assertEqual(status, 'OK')
        self.assertEqual(error_msg, "")

        #Test valid file with no walls
        filename = "test_mopbot_no_walls.txt"
        grid_dim, start_point, move_sequence, walls, status, error_msg = mopbot.fetch_values_from_file(filename)
        self.assertEqual(grid_dim, [5,5])
        self.assertEqual(start_point, [1,2])
        self.assertEqual(move_sequence, 'ENNNEWWNNSE')
        self.assertEqual(walls, [])
        self.assertEqual(status, 'OK')
        self.assertEqual(error_msg, "")


        #Test file with only two lines
        filename = "test_mopbot_2_lines.txt"
        grid_dim, start_point, move_sequence, walls, status, error_msg = mopbot.fetch_values_from_file(filename)
        self.assertEqual(grid_dim, [])
        self.assertEqual(start_point, [])
        self.assertEqual(move_sequence, '')
        self.assertEqual(walls, [])
        self.assertEqual(status, 'Error')
        self.assertEqual(error_msg, "File has Less than the minimum of three lines")

        #Test file with bad dimension
        filename = "test_mopbot_bad_dim.txt"
        grid_dim, start_point, move_sequence, walls, status, error_msg = mopbot.fetch_values_from_file(filename)
        self.assertEqual(grid_dim, [])
        self.assertEqual(start_point, [])
        self.assertEqual(move_sequence, '')
        self.assertEqual(walls, [])
        self.assertEqual(status, 'Error')
        self.assertEqual(error_msg, "Invalid dimension")

        #Test file with zero dimension
        filename = "test_mopbot_zero_dim.txt"
        grid_dim, start_point, move_sequence, walls, status, error_msg = mopbot.fetch_values_from_file(filename)
        self.assertEqual(grid_dim, [])
        self.assertEqual(start_point, [])
        self.assertEqual(move_sequence, '')
        self.assertEqual(walls, [])
        self.assertEqual(status, 'Error')
        self.assertEqual(error_msg, "Invalid dimension")


        #Test file with invalid start position
        filename = "test_mopbot_bad_start.txt"
        grid_dim, start_point, move_sequence, walls, status, error_msg = mopbot.fetch_values_from_file(filename)
        self.assertEqual(grid_dim, [])
        self.assertEqual(start_point, [])
        self.assertEqual(move_sequence, '')
        self.assertEqual(walls, [])
        self.assertEqual(status, 'Error')
        self.assertEqual(error_msg, "Invalid start position")

        #Test file with invalid movements
        filename = "test_mopbot_bad_moves.txt"
        grid_dim, start_point, move_sequence, walls, status, error_msg = mopbot.fetch_values_from_file(filename)
        self.assertEqual(grid_dim, [])
        self.assertEqual(start_point, [])
        self.assertEqual(move_sequence, '')
        self.assertEqual(walls, [])
        self.assertEqual(status, 'Error')
        self.assertEqual(error_msg, "Invalid value for move sequence")

        #Test file with zero moves
        filename = "test_mopbot_zero_moves.txt"
        grid_dim, start_point, move_sequence, walls, status, error_msg = mopbot.fetch_values_from_file(filename)
        self.assertEqual(grid_dim, [])
        self.assertEqual(start_point, [])
        self.assertEqual(move_sequence, '')
        self.assertEqual(walls, [])
        self.assertEqual(status, 'Error')
        self.assertEqual(error_msg, "Invalid value for move sequence")


        #Test file with invalid wall coordinates
        filename = "test_mopbot_bad_wall.txt"
        grid_dim, start_point, move_sequence, walls, status, error_msg = mopbot.fetch_values_from_file(filename)
        self.assertEqual(grid_dim, [])
        self.assertEqual(start_point, [])
        self.assertEqual(move_sequence, '')
        self.assertEqual(walls, [])
        self.assertEqual(status, 'Error')
        self.assertEqual(error_msg, "Invalid wall coordinates")

        #Test file with wall on start position
        filename = "test_mopbot_wall_on_start_pos.txt"
        grid_dim, start_point, move_sequence, walls, status, error_msg = mopbot.fetch_values_from_file(filename)
        self.assertEqual(grid_dim, [])
        self.assertEqual(start_point, [])
        self.assertEqual(move_sequence, '')
        self.assertEqual(walls, [])
        self.assertEqual(status, 'Error')
        self.assertEqual(error_msg, "Invalid wall coordinates")

        #Test with a filename that does not exist
        filename = "no_such_file.txt"
        grid_dim, start_point, move_sequence, walls, status, error_msg = mopbot.fetch_values_from_file(filename)
        self.assertEqual(grid_dim, [])
        self.assertEqual(start_point, [])
        self.assertEqual(move_sequence, '')
        self.assertEqual(walls, [])
        self.assertEqual(status, 'Error')
        self.assertEqual(error_msg, "File IO Error")




    def test_convert_to_pos_int(self):
        self.assertTrue(mopbot.convert_to_pos_int(['1','2']))
        self.assertFalse(mopbot.convert_to_pos_int(['1','2','3']))
        self.assertFalse(mopbot.convert_to_pos_int(['1']))
        self.assertFalse(mopbot.convert_to_pos_int(['1','a']))
        self.assertFalse(mopbot.convert_to_pos_int(['-1','2']))

    def test_coordinates_in_range(self):
        self.assertTrue(mopbot.coordinates_in_range(1,2,5,6))
        self.assertTrue(mopbot.coordinates_in_range(5,5,5,5))
        self.assertFalse(mopbot.coordinates_in_range(1,6,5,5))
        self.assertFalse(mopbot.coordinates_in_range(6,1,5,5))

    def test_check_valid_moves(self):
        self.assertTrue(mopbot.check_valid_moves('NEWSSWEEN'))
        self.assertFalse(mopbot.check_valid_moves('?'))
        self.assertFalse(mopbot.check_valid_moves('NEWST'))
        self.assertFalse(mopbot.check_valid_moves('1'))

    def test_create_grid(self):
        expected_grid = [[-1,1],[-1,1],[1,0]]
        self.assertEqual(mopbot.create_grid(2,3,[1,2],[[0,0],[0,1]]), expected_grid)
        expected_grid = [[0]]
        self.assertEqual(mopbot.create_grid(1,1,[0,0],[]), expected_grid)
        expected_grid = [[1,1],[1,1],[1,0]]
        self.assertEqual(mopbot.create_grid(2,3,[1,2],[]), expected_grid)


    def test_process_move(self):

        grid = [[-1,1],[-1,1],[1,0]]

        """
        1   0
        -1  1
        -1  1

        """

        x, y, tiles = mopbot.process_move(grid, 1, 2, "E", 0)
        self.assertEqual(mopbot.process_move(grid,1,2,"E",0), (1,2,0))
        grid = [[-1,1],[-1,1],[1,0]]
        self.assertEqual(mopbot.process_move(grid,1,2,"W",0), (0,2,1))
        grid = [[-1,1],[-1,1],[1,0]]
        self.assertEqual(mopbot.process_move(grid,1,2,"N",0), (1,2,0))
        grid = [[-1,1],[-1,1],[1,0]]
        self.assertEqual(mopbot.process_move(grid,1,2,"S",1), (1,1,2)) #changed tiles to 1, moving to new tile
        grid = [[-1,1],[-1,1],[0,0]]
        self.assertEqual(mopbot.process_move(grid,0,2,"E",1), (1,2,1)) #changed tiles to 1, moving to cleaned tile
        grid = [[-1,0],[-1,1],[1,0]]
        self.assertEqual(mopbot.process_move(grid,1,0,"E",0), (1,0,0))
        grid = [[-1,0],[-1,1],[1,0]]
        self.assertEqual(mopbot.process_move(grid,1,0,"W",0), (1,0,0))


    def test_get_post_move_coordinates_border(self):
        
        grid = [[-1,1],[-1,1],[1,0]]
        self.assertEqual(mopbot.get_post_move_coordinates_border(grid,1,2,"E"), (1,2))
        grid = [[-1,1],[-1,1],[1,0]]
        self.assertEqual(mopbot.get_post_move_coordinates_border(grid,1,2,"W"), (0,2))
        grid = [[-1,1],[-1,1],[1,0]]
        self.assertEqual(mopbot.get_post_move_coordinates_border(grid,1,2,"N"), (1,2))
        grid = [[-1,1],[-1,1],[1,0]]        
        self.assertEqual(mopbot.get_post_move_coordinates_border(grid,1,2,"S"), (1,1))
        grid = [[-1,1],[-1,1],[0,0]]        
        self.assertEqual(mopbot.get_post_move_coordinates_border(grid,0,2,"E"), (1,2))
        grid = [[-1,0],[-1,1],[1,0]]
        self.assertEqual(mopbot.get_post_move_coordinates_border(grid,1,0,"E"), (1,0))
        grid = [[-1,0],[-1,1],[1,0]]
        self.assertEqual(mopbot.get_post_move_coordinates_border(grid,1,0,"W"), (1,0))
        grid = [[0]]
        self.assertEqual(mopbot.get_post_move_coordinates_border(grid,0,0,"N"), (0,0))
        grid = [[0]]
        self.assertEqual(mopbot.get_post_move_coordinates_border(grid,0,0,"E"), (0,0))
        grid = [[0]]
        self.assertEqual(mopbot.get_post_move_coordinates_border(grid,0,0,"W"), (0,0))
        grid = [[0]]
        self.assertEqual(mopbot.get_post_move_coordinates_border(grid,0,0,"S"), (0,0))



    def test_get_post_move_coordinates(self):
        
        grid = [[-1,1],[-1,1],[1,0]]
        self.assertEqual(mopbot.get_post_move_coordinates(grid,1,2,"E"), (0,2))
        grid = [[-1,1],[-1,1],[1,0]]
        self.assertEqual(mopbot.get_post_move_coordinates(grid,1,2,"W"), (0,2))
        grid = [[-1,1],[-1,1],[1,0]]
        self.assertEqual(mopbot.get_post_move_coordinates(grid,1,2,"N"), (1,0))
        grid = [[-1,1],[-1,1],[1,0]]        
        self.assertEqual(mopbot.get_post_move_coordinates(grid,1,2,"S"), (1,1))
        grid = [[-1,1],[-1,1],[0,0]]        
        self.assertEqual(mopbot.get_post_move_coordinates(grid,0,2,"E"), (1,2))
        grid = [[-1,0],[-1,1],[1,0]]
        self.assertEqual(mopbot.get_post_move_coordinates(grid,1,0,"E"), (1,0))
        grid = [[-1,0],[-1,1],[1,0]]
        self.assertEqual(mopbot.get_post_move_coordinates(grid,1,0,"W"), (1,0))
        grid = [[0]]
        self.assertEqual(mopbot.get_post_move_coordinates(grid,0,0,"N"), (0,0))
        grid = [[0]]
        self.assertEqual(mopbot.get_post_move_coordinates(grid,0,0,"E"), (0,0))
        grid = [[0]]
        self.assertEqual(mopbot.get_post_move_coordinates(grid,0,0,"W"), (0,0))
        grid = [[0]]
        self.assertEqual(mopbot.get_post_move_coordinates(grid,0,0,"S"), (0,0))
        
    def test_mopbot(self):

        #Test valid file to see if it stays within borders
        filename = "test_mopbot_test_border.txt"
        self.assertEqual(mopbot.mop(filename), (1,0,3,''))

        #Test valid file with walls
        filename = "test_mopbot.txt"
        self.assertEqual(mopbot.mop(filename), (1,3,5,''))

        #Test file with invalid input to see if error tuple returned
        filename = "test_mopbot_bad_start.txt"
        self.assertEqual(mopbot.mop(filename), (-1,-1,0, "Invalid start position"))

        #Test valid file with walls
        filename = "generic.txt"
        self.assertEqual(mopbot.mop(filename), (6,1,27,''))

        #Test valid file with walls
        filename = "runtime.txt"
        self.assertEqual(mopbot.mop(filename), (2142,147,148,''))

        #Test valid file with walls
        filename = "edge.txt"
        self.assertEqual(mopbot.mop(filename), (-1,-1,0,'Invalid start position'))



if __name__ == '__main__':
    unittest.main()